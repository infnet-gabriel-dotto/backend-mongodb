const TYPES = {
    ListarProdutosInterface: Symbol.for("ListarProdutosInterface"),
    CriarProdutosInterface: Symbol.for("CriarProdutosInterface"),
    ExibirProdutosInterface: Symbol.for("ExibirProdutosInterface"),
    AlterarProdutosInterface: Symbol.for("AlterarProdutosInterface"),
    ProdutoRepositoryInterface: Symbol.for("ProdutoRepositoryInterface"),
    StandardMiddleware: Symbol.for("StandardMiddleware"),
    EmailServiceInterface: Symbol.for("EmailServiceInterface"),
    UsuarioRepositoryInterface: Symbol.for("UsuarioRepositoryInterface"),
    CadastroUsuarioInterface: Symbol.for("CadastroUsuarioInterface"),
};

export default TYPES;