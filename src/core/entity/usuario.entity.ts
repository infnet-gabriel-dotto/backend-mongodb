import { HistoricoEntity } from "./histCompra.entity";

export class UsuarioEntity {

    public id: string;
    public nome: string;
    public dataNascimento: string;
    public email: string;
    public senha: string;
    public historico: HistoricoEntity[];
    
    constructor(
        nome: string,
        dataNascimento: string,
        email: string,
        historico: HistoricoEntity[],
        id?: string
    ) {        
        this.nome = nome
        this.dataNascimento = dataNascimento
        this.email = email
        this.historico = historico;
        this.id = id;
    }

    static build(
        nome: string,
        dataNascimento: string,
        email: string,
        compras?: HistoricoEntity[],
        id?: string,
    ): UsuarioEntity {
        return new UsuarioEntity(
            nome,
            dataNascimento,
            email,
            compras || [],
            id,
        );
    }
}
