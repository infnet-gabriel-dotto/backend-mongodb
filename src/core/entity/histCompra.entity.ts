export class HistoricoEntity {

    public id: string;
    
    public alunoNome: string;
    public alunoId: string;
    public status: string;
    public dataCancelamento;

    constructor(
        id: string,
        alunoNome: string,
        alunoId: string,
        dataCancelamento?: string,
    ) {

        this.id = id;
        this.alunoNome = alunoNome;
        this.alunoId = alunoId;
        this.dataCancelamento = dataCancelamento;
        this.status = this.getHistoricoStatus();
    }

    private getHistoricoStatus() : string {
        return "status_mock";
    }

    static build(
        id: string,
        alunoNome: string,
        alunoId: string,
        dataCancelamento?: string,
    ): HistoricoEntity {
        return new HistoricoEntity(
            id,
            alunoNome,
            alunoId,
            dataCancelamento,
        );
    }
}
