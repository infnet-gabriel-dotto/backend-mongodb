import { HistoricoEntity } from "./histCompra.entity";

export class ProdutosEntity {

    public id: number;
    public descricao: string;
    public status: string;
    public preco: number;
    public historico: HistoricoEntity[];

    constructor(
        produtoId: number,
        descricao: string,
        preco: number,
        historico: HistoricoEntity[]
    ) {        
        this.id = produtoId;
        this.descricao = descricao;
        this.preco = preco;
        this.status = this.getProdutoStatus();
        this.historico = historico;

    }

    private getProdutoStatus() : string {
        return "status_mock";
    }

    static build(
        produtoId: number,
        descricao: string,
        preco: number,
        historico?: HistoricoEntity[],
    ): ProdutosEntity {
        return new ProdutosEntity(
            produtoId,
            descricao,
            preco,
            historico || []
        );
    }
}
