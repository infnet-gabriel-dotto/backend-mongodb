import { ProdutosEntity } from "@core/entity/produto.entity";

export type ProdutoRespositorySearchParams = {
    descricao?: string;    
}

export type ProdutoRespositoryCreateParams = {
    preco: number;    
    descricao: string;    
}

export type ProdutoRespositoryUpdateParams = {
    cursoId: string;
    data: {
        preco?: string;    
        descricao?: string;    
    }
}

export interface ProdutoRepositoryInterface {
    
    search(model: ProdutoRespositorySearchParams): Promise<ProdutosEntity[]>;

    create(model: ProdutoRespositoryCreateParams): Promise<ProdutosEntity>;

    update(model: ProdutoRespositoryUpdateParams): Promise<ProdutosEntity>;

    findById(id: string): Promise<ProdutosEntity>;
}