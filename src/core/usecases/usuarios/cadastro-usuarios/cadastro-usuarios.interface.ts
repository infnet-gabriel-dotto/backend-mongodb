export type CadastroUseCaseParams = {
    nome: string;
    email: string;
    dataNascimento: string;                
}

export interface CadastroUsuarioInterface {
    
    execute(model: CadastroUseCaseParams): Promise<any>;
}