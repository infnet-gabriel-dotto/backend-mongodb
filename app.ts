import "reflect-metadata";
import * as express from 'express';

import { Container } from "inversify";
import { InversifyExpressServer } from "inversify-express-utils";

import TYPES from "./types";

import { testControllerFactory } from "./presentation/controllers/testemiddleware.controller";
import { StandardMiddleware } from "./presentation/middlewares/standard.middleware";

import { ListarProdutosInterface } from "./core/usecases/produtos/listar-produtos/listarprodutos.interface";
import { ListarProdutosUseCase } from "./core/usecases/produtos/listar-produtos/listarprodutos.usecase";

import { CriarProdutosInterface } from "./core/usecases/produtos/criar-produtos/criarprodutos.interface";
import { CriarProdutosUseCase } from "./core/usecases/produtos/criar-produtos/criarprodutos.usecase";

import { ProdutoRepositoryInterface } from "./core/providers/data/produtos-repository.interface";
import { ProdutoRepository } from "./infra/data/repositories/produtos.repository";

const PORT = process.env.PORT || 3000;

const container = new Container();

export class App {
    
    constructor() {
        this.configDependencies();
        this.createService();        
    }
    
    configDependencies(): void {

        container.bind<ListarProdutosInterface>(TYPES.ListarProdutosInterface).to(ListarProdutosUseCase);
        container.bind<CriarProdutosInterface>(TYPES.CriarProdutosInterface).to(CriarProdutosUseCase);
        container.bind<ProdutoRepositoryInterface>(TYPES.ProdutoRepositoryInterface).to(ProdutoRepository);
        container.bind<express.RequestHandler>(TYPES.StandardMiddleware).toConstantValue(StandardMiddleware);

        testControllerFactory(container);
    }

    createService(): void {
        
        const server: InversifyExpressServer = new InversifyExpressServer(container);        
        
        server.setConfig((app) => {            

            app.use(express.json());    
        });

        server.setErrorConfig((app) => {

            app.use((err, req, res, next) => {

                if(err) {

                    if (err.name == 'BusinessError') {
                        return res.status(400).json({
                            mensagem: err.message,
                        });
                    }

                    return res.status(500).json({
                        mensagem: "Internal Server Error",
                    });
                }

                next();
            });            
        })

        const app = server.build();

        app.listen(PORT, () => {
            console.log("Servidor iniciado na porta 3000");
        });
    }
}

export default new App();
